# MEdit4CEP-iPrediceII



## Introducción

Este repositorio contiene los patrones de eventos que se han definido para detectar automáticamente y en tiempo real diversas situaciones de interés en la red de abastecimiento de agua gestionada por la empresa [Grupo Energético de Puerto Real S.A. (GEN)](https://www.grupoenergetico.es/). Este repositorio forma parte del proyecto iPREDICEII: Investigación de una PlatafoRma intEligente de mantenimiento preDICtivo de infraEstructuras - Fase II (código de referencia: AGRUP.EMP.INNOV-MINCOTUR-2022). Proyecto liderado por GEN S.A. y en colaboración con la [Universidad de Cádiz (UCA)](https://www.uca.es/), el [Clúster Andalucía Smart](https://smartcitycluster.org/), [ITelligent](https://itelligent.es/) y [Wattabit](https://wattabit.com/).

## Visión general de MEdit4CEP-iPrediceII

Para llevar a cabo la definición de estos patrones, se ha personalizado la herramienta MEdit4CEP al dominio de las redes de abastecimiento de agua. MEdit4CEP es una herramienta de modelado gráfico que facilita a los expertos en el dominio (en este caso, en gestión de agua), pero no en la tecnología de procesamiento de eventos complejos (CEP), el diseño gráfico de los patrones de eventos y su transformación automática al código de implementación, ejecutable en un motor CEP. Concretamente, la herramienta genera el código en el lenguaje de procesamiento de eventos denominado Esper EPL, proporcionado por el [motor CEP Esper](https://www.espertech.com/esper/). 
Cabe destacar que MEdit4CEP es una herramienta *open source*, desarrollada por el [Grupo de Investigación UCASE de Ingeniería del Software (TIC-025)](https://ucase.gitlab.io/) de la UCA.

## Descarga e instalación de MEdit4CEP-iPrediceII

Las instrucciones para descargar, instalar y ejecutar MEdit4CEP-iPrediceII son las siguientes: 

1. Descargar e instalar [Java SE Runtime Environment 8u](https://www.oracle.com/uk/java/technologies/javase/javase8-archive-downloads.html)

2. Descargar MEdit4CEP-iPrediceII, que está disponible en este repositorio tanto para Windows (32 y 64 bit) como para Linux (32 y 64 bit). 

3. Descomprir el archivo descargado MEdit4CEP-iPrediceII en un directorio accesible desde su máquina local. *Nota para los usuarios de Windows*: asegúrese de que extrae el archivo en un directorio cuya longitud de la ruta no exceda los 256 caracteres.

4. Abrir el ejecutable desde donde se descomprimió el archivo.

5. Seleccionar un espacio de trabajo (debe ser un directorio nuevo) y hacer clic en OK.

## Importación de los patrones de eventos modelados y generación automática de código

En este repositorio se encuentra el archivo *ipredice2water_patterns.zip*, que contiene tanto el dominio CEP modelado para la gestión del agua como todos los modelos de patrones de eventos diseñados. 

Concretamente, en la siguiente figura se muestra el dominio CEP, denominado *ipredice2water*, modelado con MEdit4CEP-iPrediceII: 

![Dominio CEP *ipredice2water* modelado con MEdit4CEP-iPrediceII](ipredice2water-pattern-screenshots/editor-CEP-domain.png)

En el siguiente listado se incluye el código Esper EPL correspondiente a dicho dominio: 

```
@public @buseventtype create json schema Measure (serialNumber String, 
  measureDateTime long, processingDateTime long, sector int, deviceType String, 
  contractType int, consumptionMode String, q1 double, q2 double, q3 double, q4 
  double, magnitude int, type String, value double, unit String)]
```

Como puede observarse, el dominio CEP se compone de un tipo de evento simple denominado *Measure*, que contiene las siguientes propiedades de eventos: *serialNumber*, *measureDateTime*, *processingDateTime*, *sector*, *deviceType*, *contractType*, *consumptionMode*, *q1*, *q2*, *q3*, *q4*, *magnitude*, *type*, *value* y *unit*.

Para importar tanto el modelo del dominio CEP como los modelos de patrones de eventos, basta con abrir la herramienta MEdit4CEP-iPrediceII y seleccionar la opción *File > Import Event Patterns...* Se deberá proporcionar el archivo *ipredice2water_patterns.zip* tal cual se ha proporcionado, esto es, **sin renombrar y sin descomprimir**. 

Para abrir cada patrón modelado, podrá hacerse uso de la opción *Event Pattern > Open...* Entonces la herramienta ofrecerá un desplegable con todos los nombres de los patrones actualmente registrados en la herramienta y deberá seleccionarse aquel que se desee visualizar, como se muestra en la siguiente figura: 

![Menú *Event Pattern* de la herramienta MEdit4CEP-iPrediceII](ipredice2water-pattern-screenshots/menu-event-pattern.png)

A modo de ejemplo, en la siguiente figura puede observarse la apertura del patrón modelado *ANRin*:

![Patrón de eventos *ANRin* modelado con la herramienta MEdit4CEP-iPrediceII](ipredice2water-pattern-screenshots/editor-event-pattern.png)

Una vez abierto el patrón en cuestión, podrá seleccionarse la opción *Event Pattern > Generate and Deploy Pattern Code* para que así la herramienta genere automáticamente el código Esper EPL correspondiente a ese patrón: 

```
@Name("ANRin")
@Tag(name="domainName", value="ipredice2water")
insert into ANRin
select a1.measureDateTime as measureDateTime, 
   a1.serialNumber as serialNumber, 
   sum(a1.value) as value, 
   a1.unit as unit
from pattern [(every a1 = Measure((a1.magnitude = 2 and a1.deviceType = 'IN')))].win:time(1 days)
```
